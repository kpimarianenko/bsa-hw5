import { combineReducers } from "redux";
import chat from '../components/Chat/reducer';
import app from '../components/App/reducer';
import modal from '../components/Modal/reducer';
import editModal from '../components/EditModal/reducer';

const rootReducer = combineReducers({
  app,
  chat,
  modal,
  editModal
});

export default rootReducer;