const API_URL = 'https://edikdolynskyi.github.io/react_sources';

async function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    .catch((error) => {
      throw error;
    });
}

export function getMessages() {
  return callApi('/messages.json', 'GET');
}

export { callApi };
