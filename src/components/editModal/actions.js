import { SET_EDITED_MESSAGE } from './actionTypes';

export const setEditedMessage = message => ({
  type: SET_EDITED_MESSAGE,
  payload: {
    message
  }
});