import { SET_EDITED_MESSAGE } from './actionTypes';

const initialState = {
  message: null
};

export default function(state = initialState, action) {
  switch(action.type) {
    case SET_EDITED_MESSAGE: {
      const { message } = action.payload;
      return {
        ...state,
        message
      };
    }

    default: {
      return state;
    }
  }
}
