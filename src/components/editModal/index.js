import React from 'react';
import './style.css';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { editMessage, deleteMessage } from '../Chat/actions';
import { hideWindow } from '../Modal/actions';
import Modal from '../Modal';

function EditModal({ message, onClose = () => {},
hideWindow, editMessage, deleteMessage, ...attrs
}) {
  const [text, setText] = useState('');

  const onChangeHandler = (event) => {
    setText(event.target.value);
  }

  const submitMessageEdit = (event) => {
    event.preventDefault();
    if (message.text.length > 0) {
      editMessage(message.id, text);
      close();
    }
  }

  const onDeleteMessage = () => {
    deleteMessage(message.id);
    close();
  }

  const close = () => {
    hideWindow();
    onClose();
  }

  useEffect(() => {
    if (message) {
      setText(message.text);
    }
  }, [message])

  return (
     <Modal onClose={onClose} {...attrs} >
      <form id="message-edit" onSubmit={submitMessageEdit}>
        <input value={text} onChange={onChangeHandler} placeholder="Write a message..." type="text" />
      </form>
      <div className="modal__controls">
        <button onClick={close} className="button button-cancel">Cancel</button>
        <i onClick={onDeleteMessage} className="far fa-trash-alt"></i>
        <input form="message-edit" type="submit" value="Edit" className="button button-confirm" />
      </div>
    </Modal>
  )
};

const mapStateToProps = (state) => {
  return {
    ...state.editModal
  }
};

const mapDispatchToProps = {
  hideWindow,
  editMessage,
  deleteMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);