import React from 'react';
import './style.css';
import { connect } from 'react-redux';
import Header from '../Header';
import Chat from '../Chat';
import Footer from './Footer';

function App({ participants, curUser }) {
  return (
      <div className="app">
        <Header user={curUser} />
        <Chat participants={participants} />
        <Footer />
      </div>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.app
  }
};

export default connect(mapStateToProps)(App);
