import { SET_CUR_USER, SET_PARTICIPANTS } from './actionTypes';

export const setCurUser = user => ({
  type: SET_CUR_USER,
  payload: {
    user
  }
});

export const setParticipants = participants => ({
  type: SET_PARTICIPANTS,
  payload: {
    participants
  }
});
