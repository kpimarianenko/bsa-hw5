import { SET_CUR_USER, SET_PARTICIPANTS } from './actionTypes';

const initialState = {
  curUser: null,
  participants: 0
}

export default function(state = initialState, action) {
  switch(action.type) {
    case SET_CUR_USER: {
      const { user } = action.payload;
      return {
        ...state,
        curUser: user
      };
    }

    case SET_PARTICIPANTS: {
      const { participants } = action.payload;
      return {
        ...state,
        participants
      };
    }

    default: {
      return state;
    }
  }
}