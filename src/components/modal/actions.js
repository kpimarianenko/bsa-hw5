import {
  SHOW_WINDOW,
  HIDE_WINDOW
} from './actionTypes';

export const showWindow = () => ({
  type: SHOW_WINDOW,
  payload: {
    isShown: true
  }
});

export const hideWindow = () => ({
  type: HIDE_WINDOW,
  payload: {
    isShown: false
  }
});