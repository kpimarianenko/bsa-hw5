import React from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import './style.css';

function Modal({ isShown, title, onClose = () => {}, hideWindow, children }) {

  const close = () => {
    hideWindow();
    onClose();
  }

  return (
    isShown ? <div className="modal__layout">
      <div className="modal">
        <div className="modal__header">
            <h3 className="modal__title">{ title }</h3>
            <span onClick={close} className="modal__close">&times;</span>
        </div>
        <div className="modal__body">{ children }</div>
      </div>
    </div> : null
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.modal
  }
};

const mapDispatchToProps = {
  ...actions
};


export default connect(mapStateToProps, mapDispatchToProps)(Modal);
