import { SHOW_WINDOW, HIDE_WINDOW } from './actionTypes';

const initialState = {
  isShown: false
};

export default function(state = initialState, action) {
  switch(action.type) {
    case SHOW_WINDOW:
    case HIDE_WINDOW: {
      const { isShown } = action.payload;
      return {
        ...state,
        isShown
      };
    }

    default: {
      return state;
    }
  }
}
