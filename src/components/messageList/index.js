import React from 'react';
import { useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { getRelativeDateString } from '../../helpers/dateHelper';
import EditModal from '../EditModal';
import Loader from '../Loader';
import MessageGroup from './MessageGroup';

function MessageList({ messages, isLoaded, ...attrs }) {
  const ref = useRef(null);
  const [messagesElements, setMessagesElements] = useState([]);

  const mapMessagesGroups = (messagesMap) => {
    const groupsElements = [];
    const mapIterator = messagesMap.keys();
    let iterator = mapIterator.next();
    while (!iterator.done) {
      const messages = messagesMap.get(iterator.value);
      groupsElements.push(<MessageGroup
        day={iterator.value}
        messages={messages}
        key={iterator.value} />)
      iterator = mapIterator.next();
    }
    return groupsElements;
  }
  
  const divideMessagesByDay = (messages) => {
    const messagesMap = new Map();
    messages.forEach((message) => {
      const createdAt = new Date(message.createdAt);
      const key = getRelativeDateString(createdAt);
      if (messagesMap.has(key)) {
        const group = messagesMap.get(key);
        group.push(message);
        messagesMap.set(key, group)
      } else {
        messagesMap.set(key, [message]);
      }
    });
    return messagesMap;
  }

  useEffect(() => {
    setMessagesElements(mapMessagesGroups(divideMessagesByDay(messages)));
  }, [messages])

  useEffect(() => {
    if (ref.current) {
      ref.current.scrollIntoView();
    }
  }, [messagesElements.length])

  return (
    <div {...attrs} className="chat__messages">
      { isLoaded ?
      (messagesElements.length > 0 ? messagesElements :
        <div>
          <p className="chat__empty" >This chat is empty</p>
          <p className="chat__empty" >You cannot send a message to an empty chat</p>
        </div>) :
      <Loader /> }
      <EditModal title="Edit" />
      <div style={{ float:"left", clear: "both" }} ref={ref}></div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.chat
  }
};

export default connect(mapStateToProps)(MessageList);
