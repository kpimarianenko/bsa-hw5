import React from 'react';
import { connect } from 'react-redux';
import { useState } from 'react';
import { addMessage } from '../Chat/actions';

function MessageInput({ isLoaded, addMessage, user }) {
  const [message, setMessage] = useState('');

  const sendMessage = (event) => {
    event.preventDefault();
    if (message.length > 0 && user && isLoaded) {
      addMessage({
        userId: user.id,
        user: user.name,
        avatar: user.avatar,
        text: message
      });
      setMessage('');
    }
  }

  const onChangeHandler = (event) => {
    setMessage(event.target.value);
  }

  return (
    <div className="chat__input">
      <form id="message-input" onSubmit={sendMessage}>
        <input value={message} onChange={onChangeHandler} placeholder="Write a message..." type="text" />
      </form>
      <input form="message-input" type="submit" value="Send" className="button button-confirm" />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.app.curUser,
    isLoaded: state.chat.isLoaded
  };
};

const mapDispatchToProps = {
	addMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);