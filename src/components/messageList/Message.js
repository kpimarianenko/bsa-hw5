import React from 'react';
import { connect } from 'react-redux';
import { getTimeFromDate } from '../../helpers/dateHelper';
import { showWindow } from '../Modal/actions';
import { setEditedMessage } from '../EditModal/actions';
import { toggleLike } from '../Chat/actions';

function Message({ message, showWindow,
  setEditedMessage, toggleLike, user
}) {
  const { id, userId, user: name, text, avatar, createdAt, isLiked } = message;
  const isCurUserMessage = user && user.id === userId

  const onEditClick = () => {
    setEditedMessage(message)
    showWindow();
  }

  const toggleLikeFunc = () => {
    toggleLike(id)
  }

  return (
    <div className="chat__message-wrapper">
      <div className={`chat__message ${isCurUserMessage ? 'message__yours' : ''}`}>
          { isCurUserMessage ? null : <img src={avatar} alt={`${name}'s ava`} />}
          <div className="message__body">
              <h4 className="message__username">{isCurUserMessage ? 'You' : name}</h4>
              <p className="message__text">{text}</p>
          </div>
          <div className="message__aside">
            <div className="message__interact">
                {!isCurUserMessage ?
                <i onClick={toggleLikeFunc} className={`${isLiked ? 'fas' : 'far'} fa-heart`}></i> :
                <i onClick={onEditClick} className="far fa-edit"></i>}
            </div>
            <p className="message__time">{getTimeFromDate(createdAt)}</p>
          </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.app.curUser
  }
};

const mapDispatchToProps = {
  showWindow,
  setEditedMessage,
  toggleLike
};

export default connect(mapStateToProps, mapDispatchToProps)(Message);