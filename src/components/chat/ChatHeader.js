import React from 'react';
import { connect } from 'react-redux';
import { getRelativeDateString, getTimeFromDate } from '../../helpers/dateHelper';

function ChatHeader({ name, participants, messages }) {
  const lastDate = messages.length ? new Date(messages[messages.length - 1].createdAt) : false;
  return (
    <div className="chat__header">
      <div className="chat__info">
        <h3>{ name }</h3>
        <p>{`${participants} participants`}</p>
        <p>{`${messages.length} messages`}</p>
      </div>
    <p>{lastDate ? 
      `Last message ${getRelativeDateString(lastDate)} at ${getTimeFromDate(lastDate)}` :
      'Chat is empty' }
    </p>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    participants: state.app.participants
  }
};

export default connect(mapStateToProps)(ChatHeader);
