const arrayHasUser = (users, id) => {
  let found = false;
  for(var i = 0; i < users.length; i++) {
    if (users[i].id === id) {
      found = true;
      break;
    }
  }
  return found;
}

export const getUsersListFromMessages = messages => {
  const users = [];
  messages.forEach(({ userId, avatar, user}) => {
    if (!arrayHasUser(users, userId)) {
      users.push({
        id: userId,
        name: user,
        avatar: avatar,
      });
    }
  });
  return users;
}