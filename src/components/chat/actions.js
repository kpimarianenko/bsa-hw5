import uuidv4 from '../../helpers/uuidv4';
import { getCurDateISO } from '../../helpers/dateHelper';
import {
  ADD_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  SET_MESSAGES,
  SET_IS_LOADED,
  TOGGLE_LIKE
} from "./actionTypes";

export const setMessages = messages => ({
  type: SET_MESSAGES,
  payload: {
    messages
  }
});

export const setIsLoaded = value => ({
  type: SET_IS_LOADED,
  payload: {
    value
  }
});

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: uuidv4(),
    data: {
      ...data,
      createdAt: getCurDateISO(),
    }
  }
});

export const editMessage = (id, text) => ({
  type: EDIT_MESSAGE,
  payload: {
    id,
    data: {
      editedAt: getCurDateISO(),
      text
    }
  }
});

export const toggleLike = (id) => ({
  type: TOGGLE_LIKE,
  payload: {
    id
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});