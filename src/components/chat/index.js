import React from 'react';
import './style.css';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUsersListFromMessages } from './service';
import { setCurUser, setParticipants } from '../App/actions';
import { getRandomInt } from '../../helpers/randomHelper';
import * as actions from './actions';
import { getMessages } from '../../helpers/apiHelper';
import ChatHeader from './ChatHeader';
import MessageList from '../MessageList';
import MessageInput from '../MessageList/MessageInput';
import { showWindow } from '../Modal/actions';
import { setEditedMessage } from '../EditModal/actions';

function Chat({ setCurUser, setParticipants, setEditedMessage,
  setMessages, setIsLoaded, messages, user, showWindow
}) {
  const editLastMessage = (event) => {
    const keyCode = 38;
    if (event.keyCode === keyCode) {
      const usersMessages = messages.filter(message => message.userId === user.id);
      if (usersMessages.length > 0) {
        event.preventDefault();
        const lastMessage = usersMessages[usersMessages.length - 1];
        setEditedMessage(lastMessage);
        showWindow();
      }
    }
  }

  useEffect(() => {
    getMessages()
    .then(messages => {
      setMessages(messages);
      setIsLoaded(true);
      return messages;
    })
    .then(messages => {
      const users = getUsersListFromMessages(messages);
      setParticipants(users.length);
      if (users.length > 0) {
        setCurUser(users[getRandomInt(0, users.length - 1)]);
      }
    });
  }, []);

  return (
    <div className="chat">
      <ChatHeader name="My chat" />
      <MessageList tabIndex="0" onKeyDown={editLastMessage} />
      <MessageInput />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.chat,
    user: state.app.curUser
  }
};

const mapDispatchToProps = {
  ...actions,
  showWindow,
  setEditedMessage,
  setCurUser,
  setParticipants
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);