export const SET_MESSAGES = "SET_MESSAGES";
export const SET_IS_LOADED = "SET_IS_LOADED";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const EDIT_MESSAGE = "EDIT_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const TOGGLE_LIKE = "TOGGLE_LIKE";