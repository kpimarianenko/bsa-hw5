import {
  ADD_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  SET_MESSAGES,
  SET_IS_LOADED, 
  TOGGLE_LIKE
} from "./actionTypes";

const initialState = {
  isLoaded: false,
  messages: []
};

export default function (state = initialState, action) {
  switch(action.type) {
    case SET_MESSAGES: {
      const { messages } = action.payload;
      return {
        ...state,
        messages
      };
    }

    case SET_IS_LOADED: {
      const { value } = action.payload;
      return {
        ...state,
        isLoaded: value
      };
    }

    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      const newMessage = {
        id,
        ...data
      }
      return {
        ...state,
        messages: [...state.messages, newMessage]
      };
    }

    case TOGGLE_LIKE: {
      const { id } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if (message.id === id) {
          return {
            ...message,
            isLiked: !message.isLiked
          }
        } else {
          return message;
        }
      });
      return {
        ...state,
        messages: updatedMessages
      };
    }

    case EDIT_MESSAGE: {
      const { id, data } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if (message.id === id) {
          return {
            ...message,
            ...data
          }
        } else {
          return message;
        }
      });
      return {
        ...state,
        messages: updatedMessages
      };
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      return {
        ...state,
        messages: state.messages.filter(message => message.id !== id)
      };
    }

    default: {
      return state;
    }
  }
}