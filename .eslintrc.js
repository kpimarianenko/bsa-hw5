module.exports = {
  "extends": ["react-app"],
  "rules": {},
  "overrides": [
    {
      "files": ["**/*.js?(x)"],
      "rules": {
        "react-hooks/exhaustive-deps": "off",
        "import/no-anonymous-default-export": "off"
      }
    }
  ]
}